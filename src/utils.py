import os
import pymongo
import uuid
import json
import requests
import firebase_admin


    

"""
auth = firebase_admin.auth

def check_firebase_session(session_token):
    try:
        decoded_token = auth.verify_session_cookie(session_token)
        # The session token is valid
        user_id = decoded_token['uid']
        print(f"Session exists for user: {user_id}")
        return True
    except auth.InvalidSessionCookieError:
        # The session token is invalid or expired
        print("Invalid session token")
        return False
"""


# = pymongo.MongoClient(host= MONGO_HOST, port=MONGO_PORT)
#conn = pymongo.MongoClient('mongodb://paperpilotdb:user@paperpilotdb:27017')

def connect_db():
    mongo_address = "16.171.29.233"
    mongo_port = "27017"
    client = pymongo.MongoClient("mongodb://user:passwd@{}:{}".format(mongo_address, mongo_port))
    #db = client.paperpilotdb
    return client

def insert_many(data, col):
    try:

        print(data)
        col_name = col
        conn = connect_db()
        if col == "users_favorites":
            res = conn.paperpilotdb.users_favorites.insert_many(data)
        else:
            res = conn.paperpilotdb.ratings.insert_many(data)
        conn.paperpilotdb.logout() 
        return True
    except Exception as e:
        print("Failed to insert in Mongo")
        print(str(e))
        return False

def insert_one(data):
    try:
        print(data)
        #db = getattr(client, MONGO_DB)
        #db.authenticate(MONGO_USERNAME, MONGO_PASSWORD)
        conn = connect_db()
        res = conn.tripdb.paths.insert_one(data)
        conn.tripdb.logout()
        return True
    except Exception as e:
        print("Failed to insert in Mongo")
        print(str(e))
        return False

def get_one(data):
    try:
        conn = connect_db()
        res = conn.paperpilotdb.papers.find_one({"id": data}, {'_id': 0, "text_extract": 0})
        if res:
            res["id"] = str(res["id"])
        else:
            res={}
        conn.paperpilotdb.logout()
        return res
    except Exception as e:
        print(str(e))
        return False

def get_all_favorites(data):
    try:
        conn = connect_db()
        print(data)
        res=list(conn.paperpilotdb.users_favorites.find({"user":data["user_id"]},{'_id': 0, "text_extract": 0}))
        results = []
        for l in res:
            #date_add = l["date_add"].strftime("%Y-%m-%d ")
            #l["date_add"] = date_add
            article = get_one(l["article"])
            l["article"] = article
            results.append(l)
        #res=list(conn.paperpilotdb.papers_test.find({"user_details.uid": data["user_id"]}, {'_id': False}))
        conn.paperpilotdb.logout()
        return results
    except Exception as e:
        print(str(e))
        return False

def get_all():
    try:
        conn = connect_db()
        print(conn)
        res=list(conn.paperpilotdb.papers.find({},{'_id': 0, "text_extract": 0}))
        results = []
        for l in res:
            #updated = l["updated"].strftime("%Y-%m-%d ")
            #published = l["updated"].strftime("%Y-%m-%d")
            #l["updated"] = updated
            #l["published"] = published
            l["id"] = str(l["id"])
            results.append(l)
        #res=list(conn.paperpilotdb.papers_test.find({"user_details.uid": data["user_id"]}, {'_id': False}))
        conn.paperpilotdb.logout()
        return results
    except Exception as e:
        print(str(e))
        return False

def delete_favorites(data):
    try:
        conn = connect_db()
        print(data)
        res = conn.paperpilotdb.users_favorites.delete_many({"user": data["user"], "article": {'$in': data["wishList"]}})
        conn.paperpilotdb.logout()
        return res
    except Exception as e:
        print(str(e))
        return False


def update_one(data):
    try:
        conn = connect_db()
        res = conn.tripdb.paths.update({"id": data["id"]}, {"$set":data})
        conn.tripdb.logout()
        print("Inserted Successfully")
        return True
    except Exception as e:
        print("Failed to insert in Mongo")
        print(str(e))
        return False

def response(status, title, data={}, error={}):
    return json.dumps({
        "status": status,
        "title": title,
        "error": error,
        "data": data
    })
