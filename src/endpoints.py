import falcon, json
from .services import  homeService, getAllPapersService, getOneArticleService,addPapersFavorisService, getUserFavorisService, addRatingService,deleteUserFavorisService
#from .utils import check_firebase_session
import json
from bson import ObjectId



class Home(object):

    #Liste des paths d'un user
    def on_get(self,req,resp):
        '''
        :param req: A request object
        :param resp: A response object
        :return:
        '''
        data = homeService()
        resp.body = json.dumps({'status': falcon.HTTP_200, 'message': 'List articles', 'data' :data })
        resp.status = falcon.HTTP_200


class Articles(object):

    def on_post(self,req,resp):
        '''
         This method will recevie the path data and  data in request body in order to store it in database
        :param req: It contains json  .......
        :param resp:
        :return:
        '''
        data = req.media
        print(data)
        listArticles = getAllPapersService(data)
        resp.body = json.dumps({'status': falcon.HTTP_200, 'message': 'List Articles get with success', 'articles': listArticles})
        resp.status=falcon.HTTP_200

    def on_get(self,req,resp):
        '''
        :param req: A request object
        :param resp: A response object
        :param user_id:  received in http path to query parcours object
        :return:
        '''
        #data = req.media
        #listArticles = getAllPapersService(data)
        listArticles = getAllPapersService()

        resp.body = json.dumps({'status': falcon.HTTP_200, 'message': 'List Articles get with success', 'articles': listArticles})
        #It will set response body as a json object of book fields.
        resp.status = falcon.HTTP_200
        #Finally return 200 response on success

class Article(object):
    #Get  one path detail
    def on_get(self, req, resp, article_id):
        '''
        :param req: A request object
        :param resp: A response object
        article_id received in http path to query  object
        :return:
        '''
        results = getOneArticleService({"id": article_id})
        resp.body = json.dumps({'status': falcon.HTTP_200, 'message': 'article details succesfully get', "article":results })
        #It will set response body as a json object of book fields.
        resp.status = falcon.HTTP_200
        #Finally return 200 response on success

        #Get  one path detail

class Favoris(object):

    def on_post(self,req,resp):
        '''
         This method will recevie the path data and  data in request body in order to store it in database
        :param req: It contains json  .......
        :param resp:
        :return:
        '''
        data = req.media
        print(data)
        #if check_firebase_session(data)
        res =  addPapersFavorisService(data)
        resp.body = json.dumps({'status': falcon.HTTP_200, 'message': 'Articles added with success', "data": res})
        resp.status=falcon.HTTP_200

    def on_get(self,req,resp):
        '''
        :param req: A request object
        :param resp: A response object
        :param user_id:  received in http path to query parcours object
        :return:
        '''
        data = req.media
        listArticles = getUserFavorisService(data)
        resp.body = json.dumps({'status': falcon.HTTP_200, 'message': 'List Articles get with success', 'articles': listArticles})
        #It will set response body as a json object of book fields.
        resp.status = falcon.HTTP_200
        #Finally return 200 response on success

class GetFavoris(object):

    def on_post(self,req,resp):
        '''
         This method will recevie the path data and  data in request body in order to store it in database
        :param req: It contains json  .......
        :param resp:
        :return:
        '''
        data = req.media
        print(data)
        #if check_firebase_session(data)
        listArticles =  getUserFavorisService(data)
        resp.body = json.dumps({'status': falcon.HTTP_200, 'message': 'Articles got with success', "data": listArticles})
        resp.status=falcon.HTTP_200


class DeleteFavoris(object):

    def on_post(self,req,resp):
        '''
         This method will recevie the path data and  data in request body in order to store it in database
        :param req: It contains json  .......
        :param resp:
        :return:
        '''
        data = req.media
        print(data)
        #if check_firebase_session(data)
        res = deleteUserFavorisService(data)
        resp.body = json.dumps({'status': falcon.HTTP_200, 'message': 'Article(s) deleted with success', "data": []})
        resp.status=falcon.HTTP_200


    
class Ratings(object):

    def on_post(self,req,resp):
        '''
        This method will recevie the path data and  data in request body in order to store it in database
        :param req: It contains json  .......
        :param resp:
        :return:
        '''
        data = req.media
        print(data)
        res = addRatingService(data)
        resp.body = json.dumps({'status': falcon.HTTP_200, 'message': 'Rating added with success', "data": res})
        resp.status=falcon.HTTP_200

    def on_get(self,req,resp):
        '''
        :param req: A request object
        :param resp: A response object
        :param user_id:  received in http path to query parcours object
        :return:
        '''
        data = req.media
        listArticles = getUserFavorisService(data)
        resp.body = json.dumps({'status': falcon.HTTP_200, 'message': 'List Articles get with success', 'articles': listArticles})
        #It will set response body as a json object of book fields.
        resp.status = falcon.HTTP_200
        #Finally return 200 response on success
