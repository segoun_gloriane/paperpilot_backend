import arxiv
import pymongo 
import boto3
import requests
import os
import PyPDF2
import io
import uuid
from  pdfminer import layout
import xml.etree.ElementTree as ET
from urllib.parse import urlparse
from utils import connect_db
from unicodedata import normalize
import re


mongo_address = "16.171.29.233"
mongo_port = "27017"

primary_category = "Computer Science"
categories_json = {
    "cs.AI": "Artificial Intelligence",
    "cs.RO": "Robotics",
    "cs.LG": "Machine Learning", 
    "cs.CV": "Computer Vision and Pattern Recognition", 
    "stat.ML":"Machine Learning"
    }
categories_list = ["cs.AI", "cs.RO", "cs.LG", "cs.CV", "stat.ML"]

def get_max_date_papers():
    #conn = pymongo.MongoClient("mongodb://user:passwd@{}:{}".format(mongo_address, mongo_port))
    conn = connect_db()
    last_document = conn.paperpilotdb.papers.find_one({}, sort=[("published", -1)])
    if last_document:
        return last_document["published"]
    else:
        return None


def ingestion_data():
    last_published_date = get_max_date_papers()
    print(last_published_date)
    categories_query = "OR".join(categories_list)
    
    search = arxiv.Search(
        query= "Artificial Intelligence  OR \"Machine Learning\" OR \"Computer Vision and Pattern Recognition\" OR \"Robotics\" OR \"Computer Science\" ",
        sort_by = arxiv.SortCriterion.SubmittedDate,
        sort_order = arxiv.SortOrder.Descending,
        max_results = 100
    )
    papers = search.results()
    if last_published_date is not  None:
        filtered_result = [result for result in papers if result.published.strftime("%d/%m/%Y")>=last_published_date ]
        return filtered_result
    else: return papers


def insert_in_db_and_s3(papers): 
    print(papers)
    list_articles = []
    list_urls = []
    for paper in papers:
        file_extension = ".pdf"
        filename = os.path.basename(paper.pdf_url) + file_extension
        author_names = [author.name for author in paper.authors ] 
        links =  [link.title for link in paper.links ] 
        list_urls.append(paper.pdf_url)
        obj = {
            "id" : str(uuid.uuid4()),
            "entry_id": paper.entry_id,
            "updated": (paper.updated).strftime("%d/%m/%Y"),
            "published":(paper.published).strftime("%d/%m/%Y"),
            "title": paper.title,
            "authors": author_names,
            "summary": paper.summary,
            "comment": paper.comment,
            "journal_ref": paper.journal_ref,
            "doi": paper.doi,
            "primary_category":paper.primary_category,
            "categories":paper.categories,
            "categories_title": [categories_json[x] for x in paper.categories if  x in categories_list ],
            "links": links,
            "s3_filename": filename,
            "pdf_url":paper.pdf_url,
            "status": "NEW"
        }
      
        list_articles.append(obj)
    
        ################# upload in S3  ######################################
    
        upload_in_s3(paper.pdf_url, filename)
    ###################insert data in mongo ################################

    # Insertion des données dans la collection
    #client = pymongo.MongoClient("mongodb://user:passwd@{}:{}".format(mongo_address, mongo_port))
    # Sélection de la base de données et de la collection
    client = connect_db()
    db = client.paperpilotdb
    collection = db.papers
    insert_result = collection.insert_many(list_articles)

    # Vérification de l'insertion réussie
    if insert_result.acknowledged:
        print("Données insérées avec succès")
    else:
        print("Erreur lors de l'insertion des données")
    return insert_result


def upload_in_s3(url, filename):
    s3 = boto3.client('s3', aws_access_key_id='AKIASREWNZ64IKEQAC4W', aws_secret_access_key='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')
    response = requests.get(url, stream=True)
    file_content = response.content
    s3.put_object(Bucket='paperpilotbucket', Key=filename, Body=file_content)
    return 200

if __name__== '__main__':

    data=ingestion_data()
    insert_in_db_and_s3(data)

