import datetime
from typing import List
from .utils import  insert_one, insert_many, get_all, get_one, get_all_favorites, delete_favorites
import uuid



API_URL = "https://api.external.citymapper.com/api/1/directions/transit"
HEADERS = {'Accept': 'application/json',
           'Citymapper-Partner-Key': ""}
def homeService():
    #result = [{'entry_id': 'http://arxiv.org/abs/2211.10959v1', 'updated': datetime.datetime(2022, 11, 20, 11, 57, 24, tzinfo=datetime.timezone.utc), 'published': datetime.datetime(2022, 11, 20, 11, 57, 24, tzinfo=datetime.timezone.utc), 'title': 'Proton quark distributions from a light-front Faddeev-Bethe-Salpeter approach', 'authors': [arxiv.Result.Author('Emanuel Ydrefors'), arxiv.Result.Author('Tobias Frederico')], 'summary': 'The projection onto the Light-Front of a Minkowski space\nFaddeev-Bethe-Salpeter equation model truncated at the valence level is applied\nto study the proton structure with constituent quarks. The dynamics of the\nmodel has built-in: (i) a bound diquark brought by a contact interaction, and\n(ii) the separation by \\Lambda_QCD of the infrared and ultraviolet interaction\nregions. The model parameters are fine tuned to reproduce the proton Dirac\nelectromagnetic form factor and mass. From that, the non-polarized longitudinal\nand transverse momentum distributions were computed. The results for the\nevolved non-polarized valence parton distributions suggest that: (i) the\nexplicit consideration of the spin degree of freedom of both quark and diquark\nseems not relevant to it, and (ii) the comparison with the global fit from the\nNNPDF4.0 calls for higher Fock components of the wave function beyond the\nvalence one.', 'comment': 'Submitted to Physics Letters B', 'journal_ref': None, 'doi': '10.1016/j.physletb.2023.137732', 'primary_category': 'hep-ph', 'categories': ['hep-ph'], 'links': [arxiv.Result.Link('http://dx.doi.org/10.1016/j.physletb.2023.137732', title='doi', rel='related', content_type=None), arxiv.Result.Link('http://arxiv.org/abs/2211.10959v1', title=None, rel='alternate', content_type=None), arxiv.Result.Link('http://arxiv.org/pdf/2211.10959v1', title='pdf', rel='related', content_type=None)], 'pdf_url': 'http://arxiv.org/pdf/2211.10959v1'}, {'entry_id': 'http://arxiv.org/abs/1409.1423v1', 'updated': datetime.datetime(2014, 9, 4, 12, 34, 14, tzinfo=datetime.timezone.utc), 'published': datetime.datetime(2014, 9, 4, 12, 34, 14, tzinfo=datetime.timezone.utc), 'title': 'The Jacobian Conjecture for the space of all the inner functions', 'authors': [arxiv.Result.Author('Ronen Peretz')], 'summary': 'We prove the Jacobian Conjecture for the space of all the inner functions in\nthe unit disc.', 'comment': None, 'journal_ref': None, 'doi': None, 'primary_category': 'math.CV', 'categories': ['math.CV'], 'links': [arxiv.Result.Link('http://arxiv.org/abs/1409.1423v1', title=None, rel='alternate', content_type=None), arxiv.Result.Link('http://arxiv.org/pdf/1409.1423v1', title='pdf', rel='related', content_type=None)], 'pdf_url': 'http://arxiv.org/pdf/1409.1423v1'}]
    return "ok"

def addPapersFavorisService(data):
    """
    """
    print(data)
    current_date = datetime.datetime.now()  
    #save in db
    list_to_add = []
    for article in data["wishList"]:
        to_save= {
            "id": str(uuid.uuid4()),
            "user": data["user_id"],
            "article": article,
            "date_add": current_date.strftime("%d/%m/%Y")
        }
        list_to_add.append(to_save)
    insert_many(list_to_add, "users_favorites")
    return "ok"

def addRatingService(data):
    """
    """
    print(data)
    current_date = datetime.datetime.now()  
    #save in db
    list_to_add = []
    for obj in data["ratings"]:
        to_save= {
            "id": str(uuid.uuid4()),
            "user": data["user_id"],
            "article": obj["article"],
            "rate": obj["rating"],
            "comment": obj["comment"],
            "date_add": current_date.strftime("%d/%m/%Y")
        }
        list_to_add.append(to_save)
    insert_many(list_to_add, "ratings")
    return "ok"

def getUserFavorisService(data):
    print(data)
    res = get_all_favorites(data)
    return res

def deleteUserFavorisService(data):
    res = delete_favorites(data)
    return res

def getAllPapersService():  
    """
    """
    resp_db = get_all()
    print(resp_db)
    return resp_db

def getOneArticleService(data):
    """
    """
    resp_db = get_one(data["id"])
    print(resp_db)
    return resp_db

