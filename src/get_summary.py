import arxiv
import pymongo 
import boto3
import requests
import os
import PyPDF2
from utils import connect_db



mongo_address = "16.171.29.233"
mongo_port = "27017"
local_directory = os.path.join(os.getcwd(), "temp_dir")


def get_new_papers():
    conn = connect_db()
    list_papers = list(conn.paperpilotdb.papers.find({"status": "NEW"}, {"s3_filename":1, "id":1, "_id":0}))
    if len(list_papers)>0:
        #response = requests.request("POST", url,  data=list_papers)
        for x in list_papers:
            file_path = download_file_from_s3(x["s3_filename"])
            doc_content = read_pdf(file_path)
            x["doc_content"] = doc_content
            response = call_model_api(x)
            update_mongo(x, response)
        # preprocess_article(article_name)
        #call model 
        #update in mongodb 
        remove_folder_contents(local_directory)

    return list_papers


def download_file_from_s3(file_key ):
    bucket_name = "paperpilotbucket"
    #bucket_name = "fasttextmodels"
    local_file_path = os.path.join(local_directory, file_key)
    s3 = boto3.client('s3', aws_access_key_id='AKIASREWNZ64IKEQAC4W', aws_secret_access_key='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')

    if not os.path.exists(local_directory):
        os.makedirs(local_directory)
        print(f"Local directory '{local_directory}' created.")

    try:
        s3.download_file(bucket_name, file_key, local_file_path)
        print(f"File downloaded from S3: s3://{bucket_name}/{file_key}")
    except Exception as e:
        print(f"Error downloading file from S3: {e}")

    return local_file_path

def remove_folder_contents(folder_path):
    for filename in os.listdir(folder_path):
        file_path = os.path.join(folder_path, filename)
        if os.path.isfile(file_path):
            os.remove(file_path)
        elif os.path.isdir(file_path):
            remove_folder_contents(file_path)
            os.rmdir(file_path)

def read_pdf(filename:str):
    with open(filename, 'rb') as f:
        pdf = PyPDF2.PdfReader(f)
        document = ' '.join([page.extract_text() for page in pdf.pages])
    return document

def extract_article_text():
    documents = []
    for filename in os.listdir(local_directory):
        f = os.path.join(local_directory, filename)
        # checking if it is a file
        if os.path.isfile(f):
            print(f"read...{f}\n")
            documents.append(read_pdf(f))
    return documents

def call_model_api(article_data):
    url = 'http://16.170.243.175:8000/getInferences'
    response = requests.post(url, data=article_data)
    return response

def update_mongo(data, modele_response):
    conn = connect_db()
    # Define the filter to match the documents you want to update
    filter = {'id': data["id"]}

    # Define the update operation
    update = {'$set': {'predicted_summary': modele_response }}

    # Update multiple documents that match the filter
    result = conn.paperpilotdb.papers.update_one(filter, update)
    return

"""
"""

def update_data():
    return

if __name__== '__main__':
    get_new_papers()
