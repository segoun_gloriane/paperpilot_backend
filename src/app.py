from .endpoints import Home, Articles, Article, Favoris, Ratings, GetFavoris, DeleteFavoris
import falcon
import firebase_admin
 
app = application = falcon.API(cors_enable=True)
# Initialize the Firebase Admin SDK
firebase_admin.initialize_app()

home = Home()
articles = Articles()
article = Article()
favoris = Favoris()
rating = Ratings()
getFavoris = GetFavoris()
deleteFavoris = DeleteFavoris()

app.add_route('/home', home)
app.add_route('/articles', articles)
app.add_route('/article/{article_id}', article)
app.add_route('/addfavoris', favoris)
app.add_route('/getfavoris', favoris)
app.add_route('/addrating', rating)
app.add_route('/getfavoris', getFavoris)
app.add_route('/deletefavoris', deleteFavoris)
#app.add_route('/article', article)
