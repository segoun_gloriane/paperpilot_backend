# Sets the base image for subsequent instructions
FROM ubuntu:20.04
# Sets the working directory in the container  
WORKDIR /app
RUN mkdir -p /app/logs
RUN apt -y update && apt install -y python3-pip
RUN apt-get update && apt-get install -y --force-yes supervisor
# Copies the dependency files to the working directory
COPY requirements.txt /app
# Install dependencies
RUN pip install -r requirements.txt
# Copies everything to the working directory
COPY . /app
RUN chmod u+x deployment/run.sh 

# Command to run on container start    
CMD ["/bin/sh", "./deployment/run.sh"]
#CMD ["gunicorn", "--reload",  "src.app", "--bind", "0.0.0.0:5000", "--access-logfile", "-", "--reload" ]